﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace MarvelousMazes
{
    public class MazeCreator
    {
        public List<string> FinishedMaze(string input)
        {
            List<string> maze = new List<string>();
            var count = 0;
            string line = "";

            
            foreach (var character in input)
            {
                if (DoesCharacterRepresentNewLine(character))
                {
                    line = AddLineToMaze(maze, line);
                }

                else if (char.IsDigit(character))
                {
                    #region Comment

                    /*****************************
                        * Interesting note here *
                        Originally, I had Convert.ToInt32(input[i]);
                        and if input was "4b" i was getting 52 from the 
                        convert. I should have gotten a 4. When I added the 
                        .ToString(), it came out correct but I don't know why the 
                        convert didnt work properly. Does it have
                        some issue with Chars vs Strings?
                        Doing a bit of research, it in fact does treat it
                        differently. If you use a char, Convert will return
                        the ASCII value. 
                        *****************************/

                    #endregion

                    count += Convert.ToInt32(character.ToString());
                }
                //now we have the count so we should start putting that many
                //characters into the list
                else
                {
                    line = ProcessCharacterForLine(character, line, count);
                    count = 0;
                }
            }
            AddLastLineToMaze(line, maze);

            return maze;
        }

        private static bool DoesCharacterRepresentNewLine(char character)
        {
            return character == '!' || character == '\n';
        }

        private static string ProcessCharacterForLine(char character, string line, int count)
        {
            if (character == 'b')
            {
                line = AddBlanksToLine(count, line);
            }
            else
            {
                line = AddCharacterToLine(character, count, line);
            }
            return line;
        }

        private static void AddLastLineToMaze(string line, List<string> maze)
        {
            if (line != "")
                maze.Add(line);
        }

        private static string AddCharacterToLine(char character, int count, string line)
        {
            for (var j = 0; j < count; j++)
            {
                line += character;
            }
            return line;
        }

        private static string AddBlanksToLine(int count, string line)
        {
            for (var j = 0; j < count; j++)
            {
                line += ' ';
            }
            return line;
        }

        private static string AddLineToMaze(List<string> maze, string line)
        {
//we dont have to add the new line in this problem
            //just add the line to the maze and go on
            maze.Add(line);
            //line has to be cleared now
            line = "";
            return line;
        }
    }
}
