﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using NUnit.Core;
using NUnit.Framework;

namespace MarvelousMazes.Tests
{
    [TestFixture]
    public class WhenEnteringAMaze
    {
        private MazeCreator _creator;

        [SetUp]
        public void Setup()
        {
            _creator = new MazeCreator();
        }

        [Test]
        public void AndTheMazeIsFiveExclamationPointsThenFiveBlankLinesShouldBeReturned()
        {
            
            //Act
            var result =  _creator.FinishedMaze("!!!!!");

            //Assert
            List<string> expected = new List<string>();
            for(var i = 0; i < 5; i++)
                expected.Add("");
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void AndTheMazeIsAFourAndABThenFourBlanksShouldBeReturned()
        {
            var result =  _creator.FinishedMaze("4b");

            List<string> expected = new List<string>();
            expected.Add("    ");

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void AndTheInputIsOneOneTThenTwoTsShouldBeReturned()
        {
            var result =  _creator.FinishedMaze("11T");

            List<string> expected = new List<string>();
            expected.Add("TT");

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void AndTheInputIsTheFirstTestCaseFromTheJudgeThenTheFirstSampleOutputShouldBeReturned()
        {
            var result =  _creator.FinishedMaze("1T1b5T!1T2b1T1b2T!1T1b1T2b2T!1T3b1T1b1T!3T3b1T!1T3b1T1b1T!5T1*1T");

            List<string> expected = new List<string>();
            /* Expected result:
            T TTTTT
            T  T TT
            T T  TT
            T   T T
            TTT   T
            T   T T
            TTTTT*T
            */
            expected.Add("T TTTTT");
            expected.Add("T  T TT");
            expected.Add("T T  TT");
            expected.Add("T   T T");
            expected.Add("TTT   T");
            expected.Add("T   T T");
            expected.Add("TTTTT*T");

            Assert.AreEqual(expected, result);
        }

        [Test]
        public void AndTheInputIsTheSecondTestInputFromTheJudgeThenTheSecondtestOutputFromTheJudgeShouldBeReturned()
        {
            var result = _creator.FinishedMaze("11X21b1X\n4X1b1X");

            List<string> expected = new List<string>();
            expected.Add("XX   X");
            expected.Add("XXXX X");

            Assert.AreEqual(expected, result);

        }
        
    }
}
